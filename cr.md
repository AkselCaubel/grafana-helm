#### Aksel

#### CAUBEL

##### RT3 Dev-Cloud

# TP Grafana-helm

La CI Gitlab execute un job pour vérifier le nombre replica 
----

Afin de crée ce job, on va devoir chercher la "value" de la "key" dans le fichier values.yaml

> La commande de parcing sera la suivante : 

```bash
grep "replicas:" values.yaml | awk -F ": " '{print $2}' # récupère la value de la key "replicas"
```

> Dans le but de ne pas utiliser de sur-couche, on va utiliser du bash pour faire la vérification : 

```bash
replicas=$(grep "replicas:" templates/deployment.yaml | awk -F ": " '{print $2}')
if [ "$replicas" -ge 3 ]; then
    echo "Le nombre de replica est suffisant";
else
    echo "ATTENTION : Il faut au moins 3 réplicas";
fi
```

> Pour afficher uniquement la template deployment, je vais utiliser l'option "-s"  -> "--show-only" 

```bash
helm template . --show-only templates/deployment.yaml
```

Le job Replica doit être lancé avant le job packaging  
----


Démontrer que le job de CI remonte une erreur si le nombre de replica est inférieur à 3.  
----


Démontrer que le job de CI remonte un succès si le nombre de replica est égale ou supérieur à 3.  
----


La vérification doit fonctionner pour les N Deployment présent dans le répertoire template.  
----


Rédiger un compte rendu de votre feature  
----



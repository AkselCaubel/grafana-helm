replicas=$(grep "replicas:" templates/deployment.yaml | awk -F ": " '{print $2}')
if [ "$replicas" -ge 3 ]; then
  echo "Le nombre de replica est suffisant";
  exit 0
else
  echo "/!\ Il faut au moins 3 replica";
  exit 1
fi